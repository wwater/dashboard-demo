﻿$(document).ready(function () {
    DefaultAjaxLoadingIcon();
    LoadRemoteData();
});

function DefaultAjaxLoadingIcon() {
    $('#AjaxLoader')
    .ajaxStart(function () {
        $(this).show();
    })
    .ajaxStop(function () {
        $(this).hide();
    });
}

function LoadRemoteData() {
    var remoteElements = $('[data-remote]')
    for (var i = 0; i < remoteElements.length; i++) {
        var remoteUrl = remoteElements[i].getAttribute("data-remote");
        var target = remoteElements[i].getAttribute("data-target");
        var dataType = "html";
        if (remoteElements[i].hasAttribute("data-datatype"))
        {
            dataType = remoteElements[i].getAttribute("data-datatype");
        }
        ReplaceTargetWithGetJsonResult(remoteUrl, target, dataType);
    }
}

function ReplaceTargetWithGetJsonResult(remoteUrl, target, dataType) {
    $(target).html("<img src=\"/Images/ajax-loader.gif\" alt=\"Loading...\" />");

    $.ajax({
        url: remoteUrl,
        dataType: dataType,
        async: true,
        data: null,
        success: function (data) {
            $(target).html(data);
        }
    });
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Web.Http;
using System.Web.Mvc;

namespace DashboardDemo.Controllers
{
    // http://stackoverflow.com/questions/4428413/why-would-multiple-simultaneous-ajax-calls-to-the-same-asp-net-mvc-action-cause
    // enable async loading for request form the same browser
    [SessionState(System.Web.SessionState.SessionStateBehavior.Disabled)]
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        //public string Get(int id)
        //{
        //    return new Random().Next(1000).ToString();
        //}

        public string GetNumberOfDays(int id)
        {
            var guid = Guid.NewGuid();
            var wait = new Random().Next(3000);
            Thread.Sleep(wait);
            return string.Format("Async result. Waited for {0} miliseconds, but it's unique, see: {1}",wait.ToString(), guid.ToString());
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
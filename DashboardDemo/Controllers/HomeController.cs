﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace DashboardDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Widget()
        {
            Thread.Sleep(new Random().Next(3000));
            var data = new Random().Next(1231);
            return PartialView(data);
        }
    }
}
